"""
შეიტანეთ მთელი რიცხვი, თუ რიცხვი არის უარყოფითი გამო¬ვიტანოთ შეტყობინება „The number is negative“, თუ რიცხვი ტოლია 0-ის გამოვიტანოთ შეტყობინება „The number equal to zero“, თუ რიცხვი არის დადებითი გამოვიტანოთ შეტყობინება „The number is positive“.
"""
a = 10
if ( a < 1 ):
    print("The number is negative")
else:
    print("The number is positive")

if ( a == 0):
    print("The number equal to zero")
else:
    print("The number is not equal to zero")