"""
შეიტანეთ რიცხვი, შეადარეთ ეს რიც¬ხვი 12,7-ს (გაითვალისწინეთ ფორმატი).
"""

a = 9
if ( a == 12 ) :
    print(True)
else:
    print(False)

if ( a != 12 ) :
    print(True)
else:
    print(False)


if ( a > 12 ) :
    print(True)
else:
    print(False)


if ( a < 12 ) :
    print(True)
else:
    print(False)


if ( a >= 12 ) :
    print(True)
else:
    print(False)

if ( a <= 12 ) :
    print(True)
else:
    print(False)