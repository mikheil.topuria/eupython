""""
1. შეიტანეთ სამი რიცხვი,
განსაზღვრეთ გამოდგება თუ არა ისინი სამკუთხედის გვერდების ზომებად. თუ გამოდგება
გამოიტანეთ ეკრანზე შეტყობინება „True“, წინააღმდეგ შემთხვევაში გამოიტანეთ შეტყობინება
„False“.

"""

a = float(input("a=>"))
b = float(input("b=>"))
c = float(input("c=>"))

if a >= 0 and b >= 0 and c >= 0 and  a + b > c and a + c > b and b + c > a:
    print("True")
else:
    print("False")


